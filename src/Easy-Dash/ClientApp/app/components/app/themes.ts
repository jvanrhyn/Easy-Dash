﻿export class Themes {

    themeList: [
        { name: 'Darkly', theme: 'darkly' },
        { name: 'Slate', theme: 'slate' },
        { name: 'Sketcky', theme: 'sketchy' }
        ];

}