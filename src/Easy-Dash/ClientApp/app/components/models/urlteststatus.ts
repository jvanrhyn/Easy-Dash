﻿export class UrlTestStatus {
    id: number;
    startedDateTime: string;
    completedDateTime: string;
    duration: string;
    statusCode: number;
    bodyContent: string;
    succeeded: boolean;
}

