﻿
export class EasyConfiguration {
    id: number;
    description: string;
    url: string;
    statusCode: number;
    bodyContains: string;
    scheduleTime: number;
    enabled: boolean;
    save() { }
}